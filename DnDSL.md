# DnDSL: Domain-specific language to describe DnD combat

# Design goals
* Easy to type quickly
  * Short commands
  * Few shifted characters
  * Optional arguments preferred to required ones
* Easy to write by hand
  * Some notes are taken by hand
* The format is primarily case-insensitive
  * Commands and participant abbreviations are case-insensitive
  * Descriptions and participant names have case maintained

# Preamble / Start of combat
The preamble describes the combat participants and declares the initiative
order. It also includes the abbreviations that will be used for each
participant. Each abbreviation and name must be unique.

```
// Comments begin with //
// Add a player (recommendation: one-character abbreviations):
+p k Kaladril
+p a Adler
+p t Takist

// Add an NPC (recommendation: two-character abbreviations):
+n gd Guard
+n sl Slave

// Add an enemy (recommendation: one-character + one-number abbreviation):
+e g1 Goblin 1
+e g2 Goblin 2
+e o1 Orc 1

// Example:
+p t Takist
+e g1 Goblin 1
+e g2 Goblin 2
+p k Kaladril
+p a Adler
+e o1 Orc
+n mr Merchant
```

## Adding participants
Participants may also be added to later rounds of combat using the
same syntax.


## Removing participants
Participants are removed by DM actions (don't do this for participant deaths)
with the `-` verb. A description may be added on the end.
```
// Remove g3 (Goblin 3) as it ran away from the combat
-e g3

// Remove mr (Merchant) as it fainted
-n mr fainted
```


# Turn description
A participant's turn description consists of the participant's abbreviation, a
two-character command, an optional target (self as default), and the description
of the action. If the target is killed during the action, append a `/` after the
target's name. If a target is optional, the first word after the digraph will be
compared to valid targets, if no matching target is found, it becomes part of
the description. If there is a numeric value associated with the action (e.g.,
damage dealt, health healed), the numeric value should be the first listed part
of the description. The description is always optional (but recommended).

Actions that affect multiple targets should be duplicated and listed twice.

## Actions available
| Action            | Digraph | Target?  | Notes                                 |
|-------------------|---------|----------|---------------------------------------|
| Attack, crit      | ac      | Yes      | Expect damage value                   |
| Attack, hit       | ah      | Yes      | Include attack type                   |
| Attack, miss      | am      | Yes      | Include spells with attack rolls      |
| Spell, success    | ss      | Optional | Include spell name                    |
| Spell, fail       | sf      | Optional | Success/fail relative to caster       |
| Healing           | hh      | Optional | Expect healing value                  |
| Movement (solely) | mm      | No       | Not listed if other actions performed |
| Ready action      | rr      | Optional |                                       |
| Dodge             | dd      | No       |                                       |
| Opportunity, crit | oc      | Yes      |                                       |
| Opportunity, hit  | oh      | Yes      |                                       |
| Opportunity, miss | om      | Yes      |                                       |
| Summon            | su      | Yes      | Target is a newly added participant   |
| Other             | ..      | Optional | Expect more description               |
    
## Examples:
```
// Kaladril attacks Goblin 1, hits for 4 damage with a scimitar
k ah g1 4 scimitar

// Kaladril attacks Orc 1, missing with scimitar
k am o1 scimitar

// Kaladril attacks Goblin 2, missing
k am g2

// Kaladril attack Goblin 1, crits for 12 damage, killing the goblin
k ac g1/

// Adler casts acid splash on Goblin 1; Goblin 1 fails its save, taking 3 damage
a ss g1 3 acid splash

// Adler casts acid splash on Goblin 2; Goblin 2 succeeds on its saving throw
a sf g2 acid splash

// Adler casts acid splash on Goblin 1 and Goblin 2
// Goblin 1 passes its saving throw
// Goblin 2 fails its save, takes 2 damage, and dies
a sf g1 acid splash
a ss g2/ 2 acid splash

// Adler casts expeditious retreat on himself
a ss expeditious retreat

// Adler casts mage armor on Kaladril
a ss k mage armor

// Kaladril heals Adler for 7 with cure wounds
k hh a 7 cure wounds

// Kaladril heals self for 5 (source omitted :( )
k hh 5

// Adler moves
a mm moves away from goblin

// Opportunity attack of Goblin 1 against Adler, misses
g1 om a

// Opportunity attack of Goblin 1 against Kaladril, hits for 5 damage from shortsword
g1 oh k 5 shortsword

// Adler readies acid splash
a rr acid splash

// Kaladril dodges
k dd
```

## Round boundary
The `--` command can be used to show the boundary between rounds


# Full example (without comments)
```
+p k Kaladril
+e g1 Goblin 1
+e g2 Goblin 2
+p a Adler

k .. shifts to wolf form
g1 mm moves closer
g2 am k shortbow
a ss g1 2 acid splash
--
k ah g1 4 bite
g1 ah k 5 shortsword
g2 ah k 4 shortbow
a sf g1 acid splash
--
k ah g1/ 5 bite
g2 ah k 4 shortbow
k .. loses wolf form
a ss g2 4 acid splash
+e o1 Orc 1
+e h1 Hobgoblin 1
--
k ah g2/ 4 thorn whip
a ss o1 5 acid splash
o1 ah a 6 orcish weapon...
h1 am k javelin
--
k am o1
a ss o1 3 acid splash
o1 ah a 4
h1 mm running away
--
k ac o1 4 scimitar
a ss o1/ 4 acid splash
h1 mm runs out of sight
-e h1

```
