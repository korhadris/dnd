#! /usr/bin/env python3

import collections
import os
import re
import sys

UmlCommand = collections.namedtuple('UmlCommand',
                                    ['actor', 'action', 'target', 'value', 'description'])

PLANT_START = '@startuml'
PLANT_END = '@enduml'

ACTIONS = ['attack_crit',
           'attack_hit',
           'attack_miss',
           'spell_success',
           'spell_fail',
           'healing',
           'movement',
           'ready',
           'dodge',
           'opportunity_crit',
           'opportunity_hit',
           'opportunity_miss',
           'summon',
           'other']

OTHER_ACTION = '..'

DNDSL_ACTIONS = ['ac',
                 'ah',
                 'am',
                 'ss',
                 'sf',
                 'hh',
                 'mm',
                 'rr',
                 'dd',
                 'oc',
                 'oh',
                 'om',
                 'su',
                 OTHER_ACTION]

PLANTUML_CONNECTIONS = ['-[#black]>>',
                        '-[#black]>',
                        '--[#black]X',
                        '-[#blue]>',
                        '--[#blue]X',
                        '-[#green]>o',
                        '->>',
                        '-\\',
                        '-->>',
                        '-[#red]>>',
                        '-[#red]>',
                        '--[#red]X',
                        '--[#green]>>',
                        '->']

DNDSL_MAP = dict(zip(DNDSL_ACTIONS, ACTIONS))

CONNECTION_MAP = dict(zip(ACTIONS, PLANTUML_CONNECTIONS))

COMMENT_RE = re.compile(r'^ *#.*')
ADD_PARTICIPANT_RE = re.compile(r'^ *\+([pnePNE]) *([^ ]+) *(.*)')
REM_PARTICIPANT_RE = re.compile(r'^ *-([pnePNE]) *([^ ]+) *(.*)')
ROUND_BREAK_RE = re.compile(r'^ *--.*')
ACTION_RE = re.compile(r'^ *([a-zA-Z][^ ]*) *([^ ]+) *([^ ]*) *([^ ]*) *(.*)')


def DndToPlant(lines):
    setup = True
    participants = []
    output = [PLANT_START, 'control DM']
    for line in lines:
        line = line.strip()
        if line == '':
            continue
        if COMMENT_RE.match(line):
            continue
        if ADD_PARTICIPANT_RE.match(line):
            p_type, p_abbrev, p_name = ADD_PARTICIPANT_RE.match(line).groups()
            p_type = p_type.lower()
            p_abbrev = p_abbrev.upper()
            if p_abbrev in participants:
                sys.stderr.write('Duplicate participant found: %s\n' % p_abbrev)
                continue
            participants.append(p_abbrev)
            if p_name:
                p_full = '"%s" as %s' % (p_name, p_abbrev)
            else:
                p_pull = p_abbrev
            if p_type == 'p':
                output.append('actor %s' % p_full)
            elif p_type == 'n':
                output.append('participant %s #green' % p_full)
            elif p_type == 'e':
                output.append('participant %s #red' % p_full)
                pass
            if not setup:
                output.append('DM %s %s **' % (CONNECTION_MAP['summon'], p_abbrev))
                pass
            output.append('activate %s' % p_abbrev)
            pass
        elif REM_PARTICIPANT_RE.match(line):
            setup = False
            _, p_abbrev, desc = REM_PARTICIPANT_RE.match(line).groups()
            p_abbrev = p_abbrev.upper()
            if p_abbrev not in participants:
                sys.stderr.write('Participant not found, but trying to remove: "%s"\n' % p_abbrev)
                continue
            output.append('DM --[#red]>> %s !! %s' % (p_abbrev, desc))
            pass
        elif ROUND_BREAK_RE.match(line):
            setup = False
            output.append('|||')
            pass
        elif ACTION_RE.match(line):
            setup = False
            # This parsing get's messy as there are many ways of having arguments
            actor, action, target, value, desc = ACTION_RE.match(line).groups()
            killed = ''
            actor = actor.upper()
            if actor not in participants:
                sys.stderr.write('Unknown participant: %s\n' % actor)
                continue
            action = action.lower()
            if action not in DNDSL_ACTIONS:
                sys.stderr.write('Unknown action: %s converting to `other` action\n' % action)
                action = OTHER_ACTION
                pass
            action = DNDSL_MAP[action]
            if target.endswith('/'):
                killed = '!!'
                target = target.rstrip('/')
            if target.upper() not in participants:
                # Assume target is self and this is the next argument, move arguments around
                desc = ('%s %s' % (value, desc)).strip()
                value = target
                target = actor
            target = target.upper()
            if value.isdigit():
                output.append('%s %s %s %s: (%s) %s' % (actor,
                                                        CONNECTION_MAP[action],
                                                        target,
                                                        killed,
                                                        value,
                                                        desc))
            else:
                output.append('%s %s %s %s: %s %s' % (actor,
                                                      CONNECTION_MAP[action],
                                                      target,
                                                      killed,
                                                      value,
                                                      desc))
                pass
            pass
        else:
            sys.stderr.write('Unable to parse line: %s\n' % line)
            pass
        pass
    output.append(PLANT_END)
    uml = '\n'.join(output)
    return uml


if __name__ == '__main__':
    if os.path.isfile(sys.argv[1]):
        uml = DndToPlant(open(sys.argv[1]).readlines())
        print(uml)
